import knex from "../services/knex";
import User from "../models/User";

const router = require("koa-router")();

router.prefix("/setting");

router
  .post("/update_profile", async ctx => {
    const { email, password, userId } = ctx.request.body;
    await User.query(knex)
      .patch({
        email,
        password
      })
      .where({ id: userId });
    ctx.body = { success: true };
  })
  .post("/apdate_dialogs_setting", async ctx => {
    const { phrases, greeting } = ctx.request.body.data || "";
    const { userId } = ctx.request.body;
    console.log(phrases, greeting, userId);
    await User.query(knex)
      .patch({ phrases: JSON.stringify(phrases), greeting })
      .where({ id: userId });

    const user = await knex("users").where({ id: userId });
    ctx.body = { user };
  });

module.exports = router;
