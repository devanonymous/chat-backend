import passport from "koa-passport";
import jwt from "jsonwebtoken";
import knex from "../services/knex";
import User from "../models/User";
import Client from "../models/Client";
import { transaction } from "objection";
import { sendForgotPassword } from "../services/nodemailer";

const router = require("koa-router")();

router.prefix("/auth");

const wrapUserWithToken = (ctx, user) => {
  if (user) {
    const token = jwt.sign(user.$toJson(), process.env.JWT_SECRET);
    ctx.body = { user, token };
  } else {
    ctx.throw(401);
  }
};

router.post("/sign_up", async ctx => {
  await transaction(knex, async tx => {
    const user = await User.query(tx).insert({
      ...ctx.request.body
    });
    wrapUserWithToken(ctx, user);
  });
});

router.post("/sign_in", async ctx => {
  await passport.authenticate("local", { session: false }, async (err, user) => {
    wrapUserWithToken(ctx, user);
  })(ctx);
});

router.get("/reset_password", async ctx => {
  const token = await User.getRandomToken();
  const email = await ctx.request.query.email;
  const user = await User.query(knex)
    .patch({ restoration_token: token })
    .where({ email });
  if (user) {
    await sendForgotPassword(email, token);
    ctx.body = { success: true };
  } else {
    ctx.throw(500);
  }
});

router.post("/update_password", async ctx => {
  await User.query(knex)
    .patch({
      password: ctx.request.body.password,
      restoration_token: null
    })
    .where({ restoration_token: ctx.request.body.token });

  ctx.body = { success: true };
});

router.post("/validate_token", async ctx => {
  const decoded = jwt.verify(ctx.request.body.token, process.env.JWT_SECRET);
  if (decoded) {
    ctx.body = { success: true };
  } else {
    ctx.throw(401);
  }
});

router.post("/clients/add", async ctx => {
  await transaction(knex, async tx => {
    await Client.query(tx).insert({
      ...ctx.request.body
    });
  });
});

module.exports = router;
