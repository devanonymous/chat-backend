import passport from "koa-passport";
import jwt from "jsonwebtoken";
import knex from "../services/knex";
import Client from "../models/Client";
import { transaction } from "objection";

const router = require("koa-router")();

router.prefix("/clients");

router.post("/sign_up", async ctx => {
  await transaction(knex, async tx => {
    await Client.query(tx).insert({
      ...ctx.request.body
    });
  });
  ctx.body = { success: true };
});

router.get("/", async ctx => {
  ctx.body = { success: true };
});

module.exports = router;
