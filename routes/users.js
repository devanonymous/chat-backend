import knex from '../services/knex'
import User from '../models/User'

const router = require('koa-router')()

router.prefix('/users')

router.post('/', async ctx => {
  const { id } = ctx.state.user
  await User.query(knex).patchAndFetchById(id, ctx.request.body)
  ctx.body = { success: true }
})

router.get('/username', async ctx => {
  const { email } = ctx.query
  const [user] = await User.query(knex).where({ email })
  if (user) {
    ctx.throw(500)
  } else {
    ctx.body = { success: true }
  }
})

export default router
