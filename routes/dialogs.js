import knex from "../services/knex";
import Dialog from "../models/Dialog";
import Client from "../models/Client";
import Message from "../models/Message";
import { transaction } from "objection";
import { sendNewDialogNotification } from "../services/oneSignal";
const router = require("koa-router")();

router.prefix("/dialogs");

// TODO: Говнокод?
// добавляет свойство с именем клиента объектам списка диалогов
const addClientsNameToDialogs = async dialogs => {
  await transaction(knex, async tx => {
    for (let key in dialogs.data) {
      if (dialogs.data[key].client_id) {
        const clientName = await Client.query(tx)
          .select("name")
          .where({ id: dialogs.data[key].client_id });
        const lastMessage = await Message.query(tx)
          .select("text")
          .where({ dialog_id: dialogs.data[key].id });
        // console.log(lastMessage[lastMessage.length - 1]);
        dialogs.data[key].client_name = clientName[0].name;
        if (lastMessage[lastMessage.length - 1]) {
          dialogs.data[key].last_message = lastMessage[lastMessage.length - 1].text;
        } else {
          dialogs.data[key].last_message = "There are no messages yet";
        }
      }
    }
  });
};

router.post("/", async ctx => {
  const body = ctx.request.body;
  await transaction(knex, async tx => {
    await Dialog.query(tx).insert({
      client_id: Number(body.client_id)
    });

    sendNewDialogNotification();
    ctx.body = { success: true };
  });
});

// получить кол-во неактивных диалогов, нужно для показа уведомления
router.get("/amount", async ctx => {
  const amount = await knex("dialogs")
    .count("*")
    .where({ user_id: null });
  ctx.body = { notActiveAmount: amount[0].count };
});

// TODO: упростить
router.get("/", async ctx => {
  const section = ctx.query.section || "getDown";
  const length = ctx.query.length || 0;
  const userId = ctx.query.userId || 0;
  let dialogs = null;
  switch (section) {
    case "getDown":
      dialogs = await knex("dialogs")
        .where({ user_id: null })
        .limit(15)
        .offset(length);
      dialogs.total = await knex("dialogs")
        .count("*")
        .where({ user_id: null });
      break;
    case "active":
      dialogs = await knex("dialogs")
        .where({ user_id: userId, is_closed: false })
        .limit(15)
        .offset(length);
      dialogs.total = await knex("dialogs")
        .count("*")
        .where({ user_id: userId, is_closed: false });
      break;
    case "saved":
      dialogs = await knex("dialogs")
        .where({ user_id: userId, is_saved: true })
        .limit(15)
        .offset(length);
      dialogs.total = await knex("dialogs")
        .count("*")
        .where({ user_id: userId, is_saved: true });
      break;
    case "closed":
      dialogs = await knex("dialogs")
        .where({ is_closed: true })
        .limit(15)
        .offset(length);
      dialogs.total = await knex("dialogs")
        .count("*")
        .where({ is_closed: true });
      break;
  }
  dialogs = { data: dialogs, total: dialogs.total[0].count };
  await addClientsNameToDialogs(dialogs);

  ctx.body = { ...dialogs };
});

// TODO: почистить говнокод
router.get("/search", async ctx => {
  await transaction(knex, async tx => {
    const section = ctx.query.section || "getDown";
    const length = ctx.query.length || 0;
    const userId = ctx.query.userId || 0;

    // const status = ctx.query.status;
    const searchingBy = decodeURI(ctx.query.searchingBy.toLowerCase());
    // const dialogs_length = await knex("dialogs_table")
    //   .count("*")
    //   .where({ status_id: status });
    const dialogs = await tx.raw(
      `with x as (
          SELECT dialogs.id,dialogs.created_at, dialogs.is_closed, dialogs.is_saved, dialogs.user_id, 
          dialogs.client_id, messages.text as last_message, clients.name as client_name FROM dialogs 
          LEFT JOIN messages on messages.dialog_id = dialogs.id
          LEFT JOIN clients on dialogs.client_id = clients.id
          WHERE true and lower(messages.text) like '%${searchingBy}%' or lower(clients.name) like '%${searchingBy}%'
        ) 
        select * from x where user_id = ${userId}`
    );
    await addClientsNameToDialogs(dialogs);
    ctx.body = { ...dialogs };
  });
});

/* добавляет диалогу id юзера (оператора), переводя диалог тем самым
   в активные для этого юзера
*/
router.post("/dialog", async ctx => {
  const dialogId = ctx.query.id;
  const userId = ctx.request.body.userId;
  const dialog = await Dialog.query(knex)
    .patch({ user_id: userId })
    .where({ id: dialogId });
  if (dialog) {
    ctx.body = { success: true };
  } else {
    ctx.throw(500);
  }
});

// перевести диалог в статус закрытых
router.post("/close", async ctx => {
  const dialogId = ctx.request.body.id;
  const dialog = await Dialog.query(knex)
    .patch({ is_closed: true })
    .where({ id: dialogId });
  if (dialog) {
    ctx.body = { success: true };
  } else {
    ctx.throw(500);
  }
});

// перевести диалог в статус сохраненных
router.post("/save", async ctx => {
  const dialogId = ctx.request.body.dialogId;
  const dialog = await Dialog.query(knex)
    .patch({ is_saved: true })
    .where({ id: dialogId });
  if (dialog) {
    ctx.body = { success: true };
  } else {
    ctx.throw(500);
  }
});

router.post("/delete", async ctx => {
  const dialogId = ctx.request.body.dialogId;
  const dialog = await Dialog.query(knex)
    .patch({ is_saved: false })
    .where({ id: dialogId });
  if (dialog) {
    ctx.body = { success: true };
  } else {
    ctx.throw(500);
  }
});

module.exports = router;
