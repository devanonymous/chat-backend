import knex from "../services/knex";
import Message from "../models/Message";
import { transaction } from "objection";
import { sendNewDialogNotification } from "../services/oneSignal";
const router = require("koa-router")();

router.prefix("/messages");

router.post("/", async ctx => {
  const { dialogId, text, isOperator } = ctx.request.body;
  await transaction(knex, async tx => {
    const message = await Message.query(tx).insert({
      dialog_id: Number(dialogId),
      text,
      is_operator: isOperator
    });
    if (message) {
      ctx.body = { success: true };
      console.log("success");
    } else {
      ctx.throw(500);
    }
  });
});

router.get("/", async ctx => {
  const { dialogId } = ctx.query;
  const messages = await knex("messages").where({ dialog_id: dialogId });
  if (messages) {
    ctx.body = { messages };
  } else {
    ctx.throw(500);
  }
});

module.exports = router;
