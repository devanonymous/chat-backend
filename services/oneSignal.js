const OneSignal = require('onesignal-node')

// create a new Client for a single app
const myClient = new OneSignal.Client({
  userAuthKey: 'MWJhNjk3NjctZWE1OC00NTRmLTkwN2QtMGFiNTZhNWJmZGRm',
  // note that "app" must have "appAuthKey" and "appId" keys
  app: {
    appAuthKey: 'MmM2YTljMDItYmYwMC00ZTMxLTg3NjEtNjk4MDQzNjYxNDI5',
    appId: '29fe98d3-d22e-4d32-b5d4-61abbf958706',
  },
})
// we need to create a notification to send
const newDialogNotification = new OneSignal.Notification({
  contents: {
    en: 'A new dialog has been added',
  },
})

// set target users
newDialogNotification.postBody['included_segments'] = ['Active Users']
newDialogNotification.postBody['excluded_segments'] = ['Banned Users']

// set notification parameters
newDialogNotification.postBody['data'] = { abc: '123', foo: 'bar' }

// send this notification to All Users except Inactive ones

export const sendNewDialogNotification = () => {
  myClient.sendNotification(newDialogNotification, function(
    err,
    httpResponse,
    data
  ) {
    if (err) {
      console.log('Something went wrong...')
    } else {
      console.log(data, httpResponse.statusCode)
    }
  })
}
