import nodemailer from 'nodemailer'

const send = async mailOptions => {
  // console.log(mailOption)
  const transporter = nodemailer.createTransport({
    service: process.env.SMTP_SERVICE,
    secure: true,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD,
    },
  })
  await transporter.sendMail(mailOptions)
}

const mailOption = (to, token) => ({
  from: process.env.SMTP_FROM,
  to,
  subject: 'Hello',
  text: `The link to reset password - ${
    process.env.WEB_APP_URL
  }/update_password?token=${token}`,
  html: `<b>The link to reset password - <a href="${
    process.env.WEB_APP_URL
  }/update_password?token=${token}">перейти</a></b>`,
})

export const sendForgotPassword = (to, token) => send(mailOption(to, token))
