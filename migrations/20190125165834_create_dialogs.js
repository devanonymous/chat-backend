exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('dialogs', function(t) {
      t.increments('id')
        .unsigned()
        .primary()
      t.timestamp('created_at')
        .notNull()
        .default(knex.fn.now())
      t.timestamp('updated_at').nullable()
      t.integer('client_id')
      t.integer('user_id')
      t.integer('rating')
      t.boolean('is_closed').default(false)
      t.boolean('is_saved').default(false)
    }),
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('dialogs')])
}
