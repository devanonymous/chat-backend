exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('messages', function(t) {
      t.increments('id')
        .unsigned()
        .primary()
      t.timestamp('created_at')
        .notNull()
        .default(knex.fn.now())
      t.timestamp('updated_at').nullable()
      t.integer('dialog_id')
      t.string('text')
      t.boolean('is_operator')
    }),
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('messages')])
}
