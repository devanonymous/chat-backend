exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table("users", function(t) {
      t.string("phrases");
      t.string("greeting");
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table("users", function(t) {
      t.dropColumn("phrases");
      t.dropColumn("greeting");
    })
  ]);
};
