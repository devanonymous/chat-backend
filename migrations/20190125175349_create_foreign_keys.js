exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table("messages", function(t) {
      t.foreign("dialog_id").references("dialogs.id");
    }),
    knex.schema.table("dialogs", function(t) {
      t.foreign("user_id").references("users.id");
      t.foreign("client_id").references("clients.id");
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table("messages_table", function(t) {
      t.dropForeign("dialog_id");
    }),
    knex.schema.table("dialogs", function(t) {
      t.dropForeign("user_id");
      t.dropForeign("client_id");
    })
  ]);
};
