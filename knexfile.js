require('dotenv').config();

const CONFIG_TEMPLATE = {
    client: 'pg',
    connection: {
        host: process.env.DATABASE_HOST,
        database: process.env.DATABASE_NAME,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASS,
        port:process.env.DATABASE_PORT,
        secretKey:process.env.JWT_SECRET
    },
    migrations: {
        directory: './migrations',
    },
};

module.exports = {
    development: CONFIG_TEMPLATE,
    production: CONFIG_TEMPLATE
};