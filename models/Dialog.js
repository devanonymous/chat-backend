import BaseModel from './BaseModel'
import User from './User'
import Client from './Client'
import { Model } from 'objection'

export default class Dialog extends BaseModel {
  static get tableName() {
    return 'dialogs'
  }

  static get pickJsonSchemaProperties() {
    return true
  }

  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['email', 'password'],
      properties: {
        id: { type: 'integer' },
        client_id: { type: 'integer' },
        user_id: { type: 'integer' },
        rating: { type: 'integer' },
        is_closed: { type: 'boolean' },
        is_saved: { type: 'boolean' },
      },
    }
  }

  static get relationMappings() {
    return {
      clients: {
        relation: Model.HasManyRelation,
        modelClass: Client,
        join: {
          from: 'clients.id',
          to: 'dialogs.client_id',
        },
      },
      users: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'users.id',
          to: 'dialogs.user_id',
        },
      },
    }
  }
}
