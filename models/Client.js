import BaseModel from './BaseModel'

export default class Client extends BaseModel {
  static get tableName() {
    return 'clients'
  }

  static get pickJsonSchemaProperties() {
    return true
  }

  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['email', 'password'], TODO: запилить
      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
      },
    }
  }
}
