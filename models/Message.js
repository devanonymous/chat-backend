import BaseModel from "./BaseModel";
import Dialog from "./Dialog";
import { Model } from "objection";

export default class Message extends BaseModel {
  static get tableName() {
    return "messages";
  }

  static get pickJsonSchemaProperties() {
    return true;
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['email', 'password'], TODO: запилить
      properties: {
        id: { type: "integer" },
        dialog_id: { type: "integer" },
        text: { type: "string" },
        is_operator: { type: "is_operator" }
      }
    };
  }

  static get relationMappings() {
    return {
      dialogs: {
        relation: Model.HasManyRelation,
        modelClass: Dialog,
        join: {
          from: "dialogs.id",
          to: "messages.dialog_id"
        }
      }
    };
  }
}
