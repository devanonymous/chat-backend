import bcrypt from "bcrypt";
import BaseModel from "./BaseModel";

const password = require("objection-password")();

export default class User extends password(BaseModel) {
  static get tableName() {
    return "users";
  }

  static getRandomToken() {
    return bcrypt.hash(new Date().valueOf().toString(), 10);
  }

  $formatJson(json) {
    const formattedJson = super.$formatJson(json);
    delete formattedJson.password;
    delete formattedJson.restoration_token;
    return formattedJson;
  }

  static get pickJsonSchemaProperties() {
    return true;
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["email", "password"],

      properties: {
        id: { type: "integer" },
        email: { type: "string" },
        password: { type: "string" },
        restoration_token: { type: ["string", "null"] },
        phrases: { type: ["string", "null"] },
        greeting: { type: ["string", "null"] }
      }
    };
  }
}

User.prototype.verifyPassword = function verifyPassword(pwd) {
  return bcrypt.compareSync(pwd, this.password);
};
