import Koa from "koa";
import cors from "koa2-cors";
import json from "koa-json";
import onerror from "koa-onerror";
import bodyparser from "koa-bodyparser";
import logger from "koa-logger";
import unless from "koa-unless";
import passport from "koa-passport";
import "./passport";

import users from "./routes/users";
import auth from "./routes/auth";
import dialogs from "./routes/dialogs";
import clients from "./routes/clients";
import messages from "./routes/messages";
import setting from "./routes/setting";

const app = new Koa();

// error handler
onerror(app);

// middlewares
app.use(
  bodyparser({
    enableTypes: ["json", "form", "text"]
  })
);

const passportJwt = passport.authenticate("jwt", { session: false });
passportJwt.unless = unless;

app.use(passport.initialize());
app.use(passport.session());
app.use(
  passportJwt.unless({
    path: [
      /^\/auth\//,
      /^\/users\/username/,
      /^\/dialogs\//,
      /^\/messages\//,
      /^\/clients\//,
      /^\/setting\//
    ]
  })
);

app.use(json());
app.use(logger());
app.use(cors());

// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

// routes
app.use(users.routes(), users.allowedMethods());
app.use(auth.routes(), auth.allowedMethods());
app.use(dialogs.routes(), dialogs.allowedMethods());
app.use(clients.routes(), clients.allowedMethods());
app.use(messages.routes(), messages.allowedMethods());
app.use(setting.routes(), setting.allowedMethods());
// error-handling
app.on("error", (err, ctx) => {
  console.error("server error", err, ctx);
});

module.exports = app;
