import passport from 'koa-passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt'
import User from './models/User'
import knex from './services/knex'
import { raw } from 'objection'

passport.serializeUser((user, done) => {
  console.log('serializeUser')
  done(null, user.id)
})

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      session: false,
    },
    async (email, password, done) => {
      const [user] = await User.query(knex).where(
        raw('lower(email)'),
        '=',
        email.toLowerCase()
      )
      console.log(user, 'local strategy')
      if (user && user.verifyPassword(password)) {
        done(null, user)
      } else {
        done(null, false)
      }
    }
  )
)

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    },
    async ({ id }, done) => {
      const [user] = await User.query(knex).where('id', '=', id)
      if (user) {
        return done(null, user)
      }
      return done(null, false)
    }
  )
)

passport.deserializeUser((id, done) => {
  console.log('deserializeUser')
  User.findById(id, (err, user) => {
    done(err, user.login)
  })
})
